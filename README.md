# pew-wp-theme

A starter for developing WordPress themes.

## Features

- laravel-mix

## Note

You need to install WordPress on a local dev server (WAMP, Local, Laragon or similar).
Then add the theme and set the `browserSync proxy` value in webpack.mix.js to match your local URL.
After that you can go ahead and run any of the provided scripts:

```
npm run dev
npm run watch
npm run build
```
