<?php
/**
 * The template for displaying the footer.
 */
 ?>

   <footer>
      <p>&copy; <?php echo Date('Y'); ?></p>
   </footer>
   <?php wp_footer(); ?>

</body>
</html>