const mix = require("laravel-mix");

mix.disableNotifications();

mix
  .js("src/scripts/app.js", "scripts")
  .sass("src/styles/global.scss", "styles")
  .setPublicPath("dist");

mix.browserSync({
  proxy: "domain.local",
  files: ["**/*.php", "src/styles/*.scss", "src/scripts/*.js"],
});
